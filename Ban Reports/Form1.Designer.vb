﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmBanReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBanReport))
        Me.btnGetBan = New System.Windows.Forms.Button()
        Me.txtScrape = New System.Windows.Forms.RichTextBox()
        Me.btnCreateReport = New System.Windows.Forms.Button()
        Me.txtWitnesses = New System.Windows.Forms.RichTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.RichTextBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.checkExtend = New System.Windows.Forms.CheckBox()
        Me.txtImageURL = New System.Windows.Forms.RichTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.boxPlayers = New System.Windows.Forms.ListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnOpenWeb = New System.Windows.Forms.Button()
        Me.btnWebReport = New System.Windows.Forms.Button()
        Me.btnSubmit = New System.Windows.Forms.Button()
        Me.btnImgUpload = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnGetBan
        '
        Me.btnGetBan.Location = New System.Drawing.Point(13, 436)
        Me.btnGetBan.Name = "btnGetBan"
        Me.btnGetBan.Size = New System.Drawing.Size(161, 60)
        Me.btnGetBan.TabIndex = 3
        Me.btnGetBan.Text = "Fetch"
        Me.btnGetBan.UseVisualStyleBackColor = True
        '
        'txtScrape
        '
        Me.txtScrape.Location = New System.Drawing.Point(12, 30)
        Me.txtScrape.Name = "txtScrape"
        Me.txtScrape.ReadOnly = True
        Me.txtScrape.Size = New System.Drawing.Size(476, 168)
        Me.txtScrape.TabIndex = 1
        Me.txtScrape.Text = ""
        '
        'btnCreateReport
        '
        Me.btnCreateReport.Enabled = False
        Me.btnCreateReport.Location = New System.Drawing.Point(180, 436)
        Me.btnCreateReport.Name = "btnCreateReport"
        Me.btnCreateReport.Size = New System.Drawing.Size(149, 60)
        Me.btnCreateReport.TabIndex = 4
        Me.btnCreateReport.Text = "Create Report"
        Me.btnCreateReport.UseVisualStyleBackColor = True
        '
        'txtWitnesses
        '
        Me.txtWitnesses.Location = New System.Drawing.Point(12, 251)
        Me.txtWitnesses.Name = "txtWitnesses"
        Me.txtWitnesses.Size = New System.Drawing.Size(470, 43)
        Me.txtWitnesses.TabIndex = 0
        Me.txtWitnesses.Text = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "RAW Data"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 306)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Full Description of What Happened"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 235)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Witnesses"
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(12, 322)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(476, 43)
        Me.txtDescription.TabIndex = 1
        Me.txtDescription.Text = ""
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(15, 213)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(67, 17)
        Me.RadioButton1.TabIndex = 8
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Jailbreak"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(120, 213)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(75, 17)
        Me.RadioButton2.TabIndex = 9
        Me.RadioButton2.Text = "Minigames"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(233, 213)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(52, 17)
        Me.RadioButton3.TabIndex = 10
        Me.RadioButton3.Text = "BHop"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(337, 436)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(149, 60)
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "Clear Form"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'checkExtend
        '
        Me.checkExtend.AutoSize = True
        Me.checkExtend.Location = New System.Drawing.Point(377, 214)
        Me.checkExtend.Name = "checkExtend"
        Me.checkExtend.Size = New System.Drawing.Size(115, 17)
        Me.checkExtend.TabIndex = 12
        Me.checkExtend.Text = "Request Extension"
        Me.checkExtend.UseVisualStyleBackColor = True
        '
        'txtImageURL
        '
        Me.txtImageURL.Location = New System.Drawing.Point(12, 387)
        Me.txtImageURL.Name = "txtImageURL"
        Me.txtImageURL.Size = New System.Drawing.Size(476, 43)
        Me.txtImageURL.TabIndex = 2
        Me.txtImageURL.Text = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 371)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(197, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Link To Image(s) Seperated By A Space"
        '
        'boxPlayers
        '
        Me.boxPlayers.FormattingEnabled = True
        Me.boxPlayers.Location = New System.Drawing.Point(517, 226)
        Me.boxPlayers.Name = "boxPlayers"
        Me.boxPlayers.Size = New System.Drawing.Size(120, 173)
        Me.boxPlayers.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(533, 210)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Recently Banned"
        '
        'btnOpenWeb
        '
        Me.btnOpenWeb.Enabled = False
        Me.btnOpenWeb.Location = New System.Drawing.Point(494, 33)
        Me.btnOpenWeb.Name = "btnOpenWeb"
        Me.btnOpenWeb.Size = New System.Drawing.Size(156, 35)
        Me.btnOpenWeb.TabIndex = 18
        Me.btnOpenWeb.Text = "Open New Post"
        Me.btnOpenWeb.UseVisualStyleBackColor = True
        '
        'btnWebReport
        '
        Me.btnWebReport.Enabled = False
        Me.btnWebReport.Location = New System.Drawing.Point(494, 74)
        Me.btnWebReport.Name = "btnWebReport"
        Me.btnWebReport.Size = New System.Drawing.Size(156, 35)
        Me.btnWebReport.TabIndex = 22
        Me.btnWebReport.Text = "Fill Report Form"
        Me.btnWebReport.UseVisualStyleBackColor = True
        '
        'btnSubmit
        '
        Me.btnSubmit.Enabled = False
        Me.btnSubmit.Location = New System.Drawing.Point(494, 156)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(156, 35)
        Me.btnSubmit.TabIndex = 23
        Me.btnSubmit.Text = "Submit Report"
        Me.btnSubmit.UseVisualStyleBackColor = True
        '
        'btnImgUpload
        '
        Me.btnImgUpload.Location = New System.Drawing.Point(517, 405)
        Me.btnImgUpload.Name = "btnImgUpload"
        Me.btnImgUpload.Size = New System.Drawing.Size(120, 91)
        Me.btnImgUpload.TabIndex = 24
        Me.btnImgUpload.Text = "Upload + Add Image"
        Me.btnImgUpload.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Enabled = False
        Me.btnPreview.Location = New System.Drawing.Point(494, 115)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(156, 35)
        Me.btnPreview.TabIndex = 25
        Me.btnPreview.Text = "Preview Report"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'lblVersion
        '
        Me.lblVersion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVersion.Location = New System.Drawing.Point(551, 499)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(117, 17)
        Me.lblVersion.TabIndex = 26
        Me.lblVersion.Text = "Error Loading Version"
        '
        'frmBanReport
        '
        Me.AcceptButton = Me.btnCreateReport
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(665, 514)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.btnImgUpload)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.btnWebReport)
        Me.Controls.Add(Me.btnOpenWeb)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.boxPlayers)
        Me.Controls.Add(Me.txtImageURL)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.checkExtend)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.RadioButton3)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtWitnesses)
        Me.Controls.Add(Me.btnCreateReport)
        Me.Controls.Add(Me.txtScrape)
        Me.Controls.Add(Me.btnGetBan)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmBanReport"
        Me.Text = "Ban Report Generator - By Yiggles Moto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGetBan As System.Windows.Forms.Button
    Friend WithEvents txtScrape As System.Windows.Forms.RichTextBox
    Friend WithEvents btnCreateReport As System.Windows.Forms.Button
    Friend WithEvents txtWitnesses As System.Windows.Forms.RichTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.RichTextBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents checkExtend As System.Windows.Forms.CheckBox
    Friend WithEvents txtImageURL As System.Windows.Forms.RichTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents boxPlayers As ListBox
    Friend WithEvents Label5 As Label
    Friend WithEvents btnOpenWeb As Button
    Friend WithEvents btnWebReport As Button
    Friend WithEvents btnSubmit As Button
    Friend WithEvents btnImgUpload As Button
    Friend WithEvents btnPreview As Button
    Friend WithEvents lblVersion As Label
End Class
