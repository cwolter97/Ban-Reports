﻿Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Text.RegularExpressions

'Created by Yiggles Moto
'To be used with Elevated Gaming Network Source Bans
'Cody Wolter - February 2016
'Version 1.2.1.2

Public Class frmBanReport
    Dim version As String = "1.2.1.2"
    Dim bannedGuyPage As String

    Dim player As String
    Dim steamID As String
    Dim invokeDate As String
    Dim banLength As String
    Dim reason As String
    Dim server As String

    Dim admin As String

    Dim bannedBadGuys(10, 10) As String


    Private Sub Scrape()
        Try
            Dim strURL As String = "http://elevatedgaming.net/sb/" & bannedGuyPage

            Dim strOutput As String = ""

            Dim wrResponse As WebResponse
            Dim wrRequest As WebRequest = HttpWebRequest.Create(strURL)

            wrResponse = wrRequest.GetResponse()
            Using SR As New StreamReader(wrResponse.GetResponseStream())
                strOutput = SR.ReadToEnd()
                ' Close and clean up the StreamReader
                SR.Close()
            End Using


            'Formatting Techniques

            ' Remove Doctype ( HTML 5 )
            strOutput = Regex.Replace(strOutput, "<!(.|\s)*?>", "")

            ' Remove HTML Tags
            ' strOutput = Regex.Replace(strOutput, "</?[a-z][a-z0-9]*[^<>]*>", "")

            ' Remove HTML Comments
            strOutput = Regex.Replace(strOutput, "<!--(.|\s)*?-->", "")

            ' Remove Script Tags
            strOutput = Regex.Replace(strOutput, "<script.*?</script>", "", RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            ' Remove Stylesheets
            strOutput = Regex.Replace(strOutput, "<style.*?</style>", "", RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            'Lets Get His Info
            player = GetInformation("Player", strOutput)
            steamID = GetInformation("Steam ID", strOutput)
            invokeDate = GetInformation("Invoked on", strOutput)
            banLength = GetInformation("Banlength", strOutput)
            reason = GetInformation("Reason", strOutput)
            'server = GetInformation("Banned from", strOutput)
            admin = GetInformation("Banned by Admin", strOutput)

            While admin.Substring(0, 1) = vbTab
                admin = admin.Substring(1, admin.Length - 1)
            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
        End Try

        txtScrape.Text = "Successfully fetched data!" & vbNewLine & vbNewLine & "Player was banned by: " & admin
        btnCreateReport.Enabled = True
    End Sub

    Private Function GetInformation(info As String, strOutput As String)
        Dim tr As Integer = InStr(strOutput, "Ban Details")
        tr = InStr(tr, strOutput, info)
        tr = InStr(tr, strOutput, "listtable_1")
        tr = InStr(tr, strOutput, ">")

        'Remove whitespace
        While (strOutput.Substring(tr, 1) = " " Or strOutput.Substring(tr, 1) = vbLf)
            tr += 1
        End While

        Dim endOfTr As Integer = InStr(tr, strOutput, "</td>")

        Dim player As String

        player = strOutput.Substring(tr, endOfTr - tr - 1)

        Dim newLine = InStr(player, vbLf)

        Try
            player = player.Substring(0, newLine - 1)
        Catch
        End Try

        If player.Contains("no nickname present") Or player.Contains("no Steam ID present") Then
            Return "Ban was created By IP"
        End If
        Return player
    End Function

    Private Sub btnGetBan_Click(sender As Object, e As EventArgs) Handles btnGetBan.Click
        btnCreateReport.Enabled = False
        bannedGuyPage = bannedBadGuys(boxPlayers.SelectedIndex + 1, 0)
        txtScrape.Text = String.Empty
        txtScrape.Text = "Extracting..." & Environment.NewLine
        Scrape()
    End Sub

    Private Sub btnCreateReport_Click(sender As Object, e As EventArgs) Handles btnCreateReport.Click
        Dim extend As String = String.Empty
        Dim imageURLs(10) As String
        Dim holderOne As Integer
        Dim holderTwo As Integer
        Dim counter As Integer

        If checkExtend.Checked = True Then
            extend = "[Request Permanent]"
        End If

        For Each myControl As RadioButton In Me.Controls.OfType(Of RadioButton)()
            If myControl.Checked Then
                server = myControl.Text
            End If
        Next

        txtScrape.Text = "[b][color=#00FF00]Name:[/color][/b] " & player & vbNewLine &
                        "[b][color=#00FF00]Steam ID (or IP):[/color][/b] " & steamID & vbNewLine &
                        "[b][color=#00FF00]Duration of ban:[/color][/b] " & banLength & extend & vbNewLine &
                        "[b][color=#00FF00]Time ban was invoked:[/color][/b] " & invokeDate & vbNewLine &
                        "[b][color=#00FF00]Server: [/color][/b] " & server & vbNewLine &
                        "[b][color=#00FF00]Reason:[/color][/b] " & reason & vbNewLine &
                        "[b][color=#00FF00]Witnesses:[/color][/b] " & txtWitnesses.Text & vbNewLine &
                        "[b][color=#00FF00]Full description of what happened:[/color][/b] " & txtDescription.Text & vbNewLine &
                        "[b][color=#00FF00]Source Bans Link:[/color][/b] " & "http://elevatedgaming.net/sb/" & bannedGuyPage

        If txtImageURL.Text <> String.Empty Then
            If InStr(txtImageURL.Text, " ") = 0 Then
                imageURLs(0) = txtImageURL.Text
            Else
                Do
                    holderTwo = holderOne
                    holderOne = InStr(holderOne + 1, txtImageURL.Text, " ")

                    Try
                        imageURLs(counter) = txtImageURL.Text.Substring(holderTwo, holderOne - holderTwo - 1)
                    Catch ex As Exception
                        imageURLs(counter) = txtImageURL.Text.Substring(holderTwo, txtImageURL.Text.Length - holderTwo)
                    End Try


                    counter += 1
                Loop Until holderOne = 0
            End If


            txtScrape.Text += vbNewLine

            For Each element As String In imageURLs
                If element <> String.Empty Then
                    txtScrape.Text += vbNewLine & "[img]" & element & "[/img]"
                End If
            Next

        End If

        btnOpenWeb.Enabled = True

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtScrape.Clear()
        txtWitnesses.Clear()
        txtDescription.Clear()
        txtImageURL.Clear()
        RadioButton1.Checked = True
        checkExtend.Checked = False
    End Sub

    Private Sub frmBanReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            Dim strURL As String = "http://elevatedgaming.net/sb/index.php?p=home"

            Dim strOutput As String = ""

            Dim wrResponse As WebResponse
            Dim wrRequest As WebRequest = HttpWebRequest.Create(strURL)

            wrResponse = wrRequest.GetResponse()

            Using sr As New StreamReader(wrResponse.GetResponseStream())
                strOutput = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using


            'Formatting Techniques

            ' Remove Doctype ( HTML 5 )
            strOutput = Regex.Replace(strOutput, "<!(.|\s)*?>", "")

            ' Remove HTML Tags
            ' strOutput = Regex.Replace(strOutput, "</?[a-z][a-z0-9]*[^<>]*>", "")

            ' Remove HTML Comments
            strOutput = Regex.Replace(strOutput, "<!--(.|\s)*?-->", "")

            ' Remove Script Tags
            strOutput = Regex.Replace(strOutput, "<script.*?</script>", "", RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            ' Remove Stylesheets
            strOutput = Regex.Replace(strOutput, "<style.*?</style>", "", RegexOptions.Singleline Or RegexOptions.IgnoreCase)

            ' Lets get these guys from the ban list
            Dim tr As Integer = InStr(strOutput, """front-module"" style=""float:left""")
            For counter As Integer = 1 To 10

                tr = InStr(tr, strOutput, "index.php?p=banlist&advSearch")

                Dim endOfTr As Integer = InStr(tr, strOutput, ";")

                bannedGuyPage = strOutput.Substring(tr - 1, endOfTr - tr - 1)

                For i As Integer = 0 To 2
                    tr = InStr(tr + 1, strOutput, "listtable_1")
                Next

                tr = InStr(tr, strOutput, ">")
                endOfTr = InStr(tr, strOutput, "</td>")

                While (strOutput.Substring(tr, 1) = " " Or strOutput.Substring(tr, 1) = vbLf)
                    tr += 1
                End While

                Dim bannedGuyName As String

                bannedGuyName = strOutput.Substring(tr, endOfTr - tr - 1)

                Dim newLine = InStr(bannedGuyName, vbLf)

                Try
                    bannedGuyName = bannedGuyName.Substring(0, newLine - 1)
                Catch
                End Try

                If bannedGuyName.Contains("no nickname present") Then
                    bannedGuyName = "Banned By IP or Steam ID"
                End If

                bannedBadGuys(counter, 0) = bannedGuyPage
                bannedBadGuys(0, counter) = bannedGuyName


            Next

        Catch ex As Exception

            MessageBox.Show(ex.Message, "Error")

        End Try

        Try
            For counter As Integer = 1 To 10
                boxPlayers.Items.Add(bannedBadGuys(0, counter))
            Next

            boxPlayers.SelectedIndex = 0
        Catch ex As Exception

        End Try

        Try
            'Check for updates
            Dim versionURL As String = "https://dl.dropboxusercontent.com/u/10875071/Programs/version.txt"
            Dim programURL As String = "https://dl.dropboxusercontent.com/u/10875071/Programs/Ban%20Reports.exe"

            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(versionURL)
            Dim response As System.Net.HttpWebResponse = request.GetResponse()

            Dim stream As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream)

            Dim newVersion As String = stream.ReadToEnd()

            If newVersion.Contains(version) Then
                '
            Else
                download.Show()
                download.lblUpdateText.Text = "An update is available to version " & newVersion & "!"
            End If
        Catch ex As Exception
            MessageBox.Show("Error Checking For Update")
        End Try

        lblVersion.Text = "Version: " & version


    End Sub

    Private Sub btnOpenWeb_Click(sender As Object, e As EventArgs) Handles btnOpenWeb.Click


        btnWebReport.Enabled = True

        Form2.Show()
        Form2.WebBrowser1.Navigate("http://elevatedgaming.net/forums/posting.php?mode=post&f=115")
    End Sub

    Private Sub btnWebReport_Click(sender As Object, e As EventArgs) Handles btnWebReport.Click

        For Each element As HtmlElement In Form2.WebBrowser1.Document.All
            If InStr(element.Name, "subject") Then
                If checkExtend.Checked Then
                    element.SetAttribute("value", "BR: " & player & " [extend]")
                Else
                    element.SetAttribute("value", "BR: " & player)
                End If

            End If
            If InStr(element.Name, "message") Then
                element.SetAttribute("value", txtScrape.Text)
            End If
        Next

        btnSubmit.Enabled = True
        btnPreview.Enabled = True
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        For Each element As HtmlElement In Form2.WebBrowser1.Document.All
            If InStr(element.Name, "post") Then
                element.InvokeMember("click")
            End If
        Next
    End Sub

    Dim clientID As String = "da8b636a7aed8da"
    Public Function UploadImage(ByVal image As String)
        Dim w As New WebClient()
        w.Headers.Add("Authorization", "Client-ID " & clientID)
        Dim keys As New System.Collections.Specialized.NameValueCollection

        Try
            keys.Add("image", Convert.ToBase64String(File.ReadAllBytes(image)))
            Dim responseArray As Byte() = w.UploadValues("https://api.imgur.com/3/image", keys)
            Dim result = Encoding.ASCII.GetString(responseArray)
            Dim reg As New System.Text.RegularExpressions.Regex("link"":""(.?)""")
            Dim match As RegularExpressions.Match = reg.Match(result)
            'Dim url As String = match.ToString.Replace("link"":""", "").Replace("""", "").Replace("\/", "/")

            Dim url As String = result.ToString
            Dim holder1 As Integer = InStr(url, "id")
            holder1 = InStr(holder1 + 1, url, """")
            holder1 = InStr(holder1 + 1, url, """")
            Dim holder2 As Integer = InStr(holder1 + 1, url, """") - 1

            url = url.Substring(holder1, holder2 - holder1)

            Return "http://imgur.com/" & url & ".png "
        Catch ex As Exception
            MessageBox.Show("Error Uploading")
        End Try

    End Function

    Private Sub btnImgUpload_Click(sender As Object, e As EventArgs) Handles btnImgUpload.Click
        Dim filepath As New OpenFileDialog
        Dim pathOfFile As String

        filepath.Title = "Open Capture"


        If filepath.ShowDialog = Windows.Forms.DialogResult.OK Then
            pathOfFile = (Path.GetDirectoryName(filepath.FileName))
        End If

        pathOfFile = (filepath.FileName)
        Dim url As String = UploadImage(pathOfFile)

        txtImageURL.Text += url
    End Sub

    Private Sub btnPreview_Click(sender As Object, e As EventArgs) Handles btnPreview.Click
        For Each element As HtmlElement In Form2.WebBrowser1.Document.All
            If InStr(element.Name, "preview") Then
                element.InvokeMember("click")
            End If
        Next
    End Sub
End Class
